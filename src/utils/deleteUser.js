import { DELETE_USER } from "../redux/types";

export const deleteAdmin = (id, deletePerson) => {
    deletePerson('delete', `admin/users/${id}`, DELETE_USER);
}