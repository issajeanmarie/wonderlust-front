import { CHANGE_ROLE } from '../redux/types';

const changeRole = (email, role, changeUserRole) => {
    role === 'traveller' && 
    changeUserRole('patch', `admin/users/${email}`, CHANGE_ROLE, {"role": "travelAdmin"})
}

export default changeRole