function Logout() {
    localStorage.removeItem('wonderLustToken')
    window.location.href = '/'
}

export default Logout
