import './index.css'
import { RightOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'

const SingleCard = ({status, name, price, id, picture}) => {
    return(
        <div className="trip_card">
            <div className="card_cover">
                <img src={picture} alt="card holder"/>
            </div>
            <div className="card_desc">
                <div style={{width: '100%'}}>
                    <p className="title_4 card_title text_label">{name}</p>
                    <p className="title_2" style={{marginTop: '-10px'}}>{price?.toLocaleString()} Rwf</p>
                </div>
                <p className={'bold '+status}>{status || null}</p>
                <Link to={`/places/${id}`}>
                    <button className="card_button">
                        <RightOutlined />
                    </button>
                </Link>
            </div>
        </div>
    )
}

export default SingleCard