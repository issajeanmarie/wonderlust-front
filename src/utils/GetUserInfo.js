import jwt_decode from "jwt-decode";

export function GetUserInfo() {
    let token = localStorage.getItem('wonderLustToken');
    let userData = jwt_decode(token)
    return(userData.user)
}
