import React from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
import {Forgot_Password}  from '../Forgot_Password'
import { act } from 'react-dom/test-utils'
import { BrowserRouter } from 'react-router-dom'

let container = null;
beforeEach(() => {
    //Prepare the rendering area
    container = document.createElement('div')
    document.body.appendChild(container)
})

afterEach(() => {
    //Clean up area after each test
    unmountComponentAtNode(container)
    container.remove()
    container = null
})

describe("Forgot password", () => {
    it("Renders  without crashing", () => {
        act(() => {
            render(<BrowserRouter><Forgot_Password /></BrowserRouter>, container)
        })
        expect(container.textContent).toContain('Signup')
        
    })
    
})