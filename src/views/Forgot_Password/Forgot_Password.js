import React from 'react'
import { Input } from 'antd/lib';
import Logo from '../../assets/img/Group 5.png'
import './index.css'
import { Link } from 'react-router-dom'
import { useFormik } from 'formik'
import * as Yup from 'yup'

export function Forgot_Password() {

    const formik = useFormik({
        initialValues: {
            index: '343',
            email: '',
        },
        validationSchema: Yup.object({
            email: Yup.string().email().required('Email required')
        }),
        onSubmit: values => {
            alert(JSON.stringify(values))
        }
    })

    return (
        <div className="login">
            <div className="loginform">

                <div className="signup_logo">
                    <img src={Logo} alt="Logo"/>
                </div>

                <p className="title_1 text_center signup_label">Forgot password?</p>
                <p className="title_6 text_center text_label">Provide your email below to recover your password.</p>
                <br />

                <form onSubmit={formik.handleSubmit}>
                    <Input 
                    {...formik.getFieldProps('email')}
                    type="email"
                    name="email"
                    placeholder="Email" 
                    className="input" 
                    />
                    {formik.touched.email && formik.errors.email ?
                    <span className="error_msg">{formik.errors.email}</span> : null}

                    <button type="submit" className="btn_primary bg_orange">Signup</button>
                </form>

                <Link to="/">
                    <p className="title_5 text_center">Login</p>
                </Link>
            </div>
        </div>
    )
}
