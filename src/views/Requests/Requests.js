import { Layout, Spin } from 'antd/lib';
import { useEffect } from 'react';
import { connect } from 'react-redux';
import Profile_Picture from '../../assets/img/gollira.jpg'
import { 
    Navbar, 
    RequestsMenu,
    SingleRequest
} from '../../components'
import apiAction from '../../redux/actions/apiAction';
import { GetUserInfo } from '../../utils/GetUserInfo';

const { Content } = Layout;
function Requests({getRequests, requestsGot, approveRequest, deleteRequest}) {

    //Log me out
    if(!localStorage.getItem('wonderLustToken')){
        window.location.href = '/'
    }

    useEffect(() =>{
        getRequests('get', 'requests/travelAdmin', 'GET_REQUESTS')
    }, [])

    return (
        <Layout>            
            <Navbar 
                names="Issa Jean Marie" 
                role={`${GetUserInfo()?.user?.role === 'traveller' ? '' :
                    GetUserInfo()?.user?.role === 'travelAdmin' ? 'Travel admin' : 'Super admin'
                    }`}  
                RequestsMenu={GetUserInfo()?.user?.role === 'traveller' ? RequestsMenu : ''}
                Profile_Picture={Profile_Picture} 
            />

            <Content className="contents">
                <br /><br />
                <p className="title_2" style={{marginBottom: '30px'}}>Trip requests</p>
                {
                    requestsGot?.data && requestsGot?.data?.length && requestsGot?.data.map(result => 
                        
                            <SingleRequest
                                name={result.place.title}
                                price={result.place.price}
                                user_name={result.requestor.firstName ,' ', result.requestor.lastName}
                                user_email={result.requestor.email}
                                picture={result.place.picture}
                                id={result.id}
                                approveRequest={approveRequest}
                                deleteRequest={deleteRequest}
                                key={result.id}
                                status={result.status}
                            />
                    )
                } 

                {requestsGot.data.isLoading && <Spin style={{display:'block', marginTop: 'auto'}} />}

            </Content>
        </Layout>
    )
}

const mapState = ({ requests }) => ({requestsGot : requests});
export default connect(mapState, {
    getRequests: apiAction,
    approveRequest: apiAction,
    deleteRequest: apiAction
})(Requests)