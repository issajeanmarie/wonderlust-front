import React from 'react'
import { Input, Spin } from 'antd/lib';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import Logo from '../../assets/img/Group 5.png'
import './index.css'
import { Link } from 'react-router-dom'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { connect } from 'react-redux';
import { USER_REGISTER } from '../../redux/types'
import apiAction from '../../redux/actions/apiAction';

function Register({addUser, register}) {

    const formik = useFormik({
        initialValues: {
            email: '',
            firstName: '',
            lastName: '',
            password: ''
        },
        validationSchema: Yup.object({
            firstName: Yup.string().required('First name required'),
            lastName: Yup.string().required('Last name required'),
            email: Yup.string().email().required('Email required'),
            password: Yup.string().required('Password required')
        }),
        onSubmit: values => {
            addUser('post', 'auth/signup', USER_REGISTER, JSON.stringify(values))
        }
    })

    return (
        <div className="login">
            <div className="loginform">

                <div className="signup_logo">
                    <img src={Logo} alt="Logo"/>
                </div>

                <p className="title_1 text_center signup_label">Get on board</p>
                <p className="title_6 text_center text_label">Travel around the world with us.</p>
                <br />
                
                <form onSubmit={formik.handleSubmit}>
                    <Input
                        {...formik.getFieldProps('firstName')}
                        type="text"
                        name="firstName"
                        className="input"
                        placeholder="First name"
                    />
                    {formik.touched.firstName && formik.errors.firstName ?
                    <span className="error_msg">{formik.errors.firstName}</span> : null}

                    <Input
                        {...formik.getFieldProps('lastName')}
                        type="text"
                        name="lastName"
                        className="input"
                        placeholder="Last name"
                    />
                    {formik.touched.lastName && formik.errors.lastName ?
                    <span className="error_msg">{formik.errors.lastName}</span> : null}

                    <Input
                        {...formik.getFieldProps('email')}
                        type="email"
                        name="email"
                        className="input"
                        placeholder="Email"
                    />
                    {formik.touched.email && formik.errors.email ?
                    <span className="error_msg">{formik.errors.email}</span> : null}

                    <Input.Password
                        {...formik.getFieldProps('password')}
                        name="password"
                        className="input"
                        placeholder="Password"
                        iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                    />
                    {formik.touched.password && formik.errors.password ?
                    <span className="error_msg">{formik.errors.password}</span> : null}

                    <Input.Password
                        name="confirmpassword"
                        className="input"
                        placeholder="Confirm password"
                        iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                    />              

                    <button type="submit" className="btn_primary bg_orange">Sign up</button>                        
                </form>

                <Link to="/">
                    <p className="title_5 text_center">Login</p>
                </Link>
                {
                    register.isLoading && <Spin style={{margin: 'auto', display: 'block'}} />
                }
            </div>
        </div>
    )
}

const mapState = ({ register }) => ({register});
export default connect(mapState, {
    addUser: apiAction,
})(Register)


