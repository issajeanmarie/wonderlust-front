import React from 'react'
import { Layout } from 'antd/lib';
import Profile_Picture from '../../assets/img/gollira.jpg'
import { 
    Navbar, 
    RequestsMenu,
    ShowPlaces,
    PlaceDetails
} from '../../components'
import { Route } from 'react-router-dom'
import { GetUserInfo } from '../../utils/GetUserInfo'

const { Content } = Layout;
function Places() {    

    //Log me out
    if(!localStorage.getItem('wonderLustToken')){
        window.location.href = '/'
    }

    return (
        <Layout>            
            <Navbar 
                names={`${GetUserInfo()?.user?.firstName} ${GetUserInfo()?.user?.lastName}`} 
                role={`${GetUserInfo()?.user?.role === 'traveller' ? '' :
                    GetUserInfo()?.user?.role === 'travelAdmin' ? 'Travel admin' : 'Super admin'
                    }`}
                RequestsMenu={GetUserInfo()?.user?.role === 'traveller' ? RequestsMenu : ''}
                Profile_Picture={Profile_Picture} 
            />
            <Content className="contents">
                <Route path="/places" exact>
                    <ShowPlaces/>
                </Route>
                <Route path="/places/:id">
                    <PlaceDetails/>
                </Route>

            </Content>
        </Layout>
    )
}

export default Places