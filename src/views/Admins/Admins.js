import React, { useEffect } from 'react'
import { Layout } from 'antd/lib';
import { Route } from 'react-router-dom'
import { 
    Navbar, 
    SuperAdminNavbar,
    AdminsTable,
    Users
} from '../../components'
import apiAction from '../../redux/actions/apiAction';
import { connect } from 'react-redux';
import { GET_ADMINS, GET_USERS } from '../../redux/types';
const { Content } = Layout;

function Admins({getAdmins, getUsers, admins, users, role, deletePerson, changeUserRole}) {
    
    //Log me out
    if(!localStorage.getItem('wonderLustToken')){
        window.location.href = '/'
    }

    useEffect(() => {
        getAdmins('get', 'admin/users?type=travelAdmin', GET_ADMINS)
        getUsers('get', 'admin/users?type=users', GET_USERS)
     }, [])

    const adminsResult = !admins.isLoading && admins.data.length >= 1 ? admins.data : []
    const usersResult = !users.isLoading && users.data.length >= 1 ? users.data : []

    return (
        <Layout> 
            <Navbar />
            <SuperAdminNavbar  />

            <Content className="contents">
                <Route path="/super_admin/" exact>
                    <br /><br />
                    <p className="title_2">Trip admins ({adminsResult.length})</p>
                    <br />
                    <AdminsTable admins={adminsResult} deletePerson={deletePerson} />
                </Route>
                <Route path="/super_admin/users" exact>
                    <br /><br />
                    <p className="title_2">Users ({usersResult.length})</p>
                    <br />
                    <Users allUsers={usersResult} changeUserRole={changeUserRole} />
                </Route>
            </Content>
        </Layout>
    )
}

const mapState = ({ admins, deleteUser, users, role }) => ({admins, deleteUser, users, role});
export default connect(mapState, {
    getAdmins: apiAction,
    deletePerson: apiAction,
    getUsers: apiAction,
    changeUserRole: apiAction
})(Admins)
