import { Layout, Breadcrumb, Spin } from 'antd/lib';
import { 
    Navbar, 
    RequestsMenu
} from '../../components'
import Profile_Picture from '../../assets/img/gollira.jpg'
import { SingleCard } from '../../utils'
import './index.css'
import apiAction from '../../redux/actions/apiAction';
import { useEffect } from 'react';
import { GET_REQUESTS_STATUS } from '../../redux/types';
import { connect } from 'react-redux';
import { GetUserInfo } from '../../utils/GetUserInfo';
import { Link } from 'react-router-dom';
const { Content } = Layout;

function RequestsStatus({requestStatus, getRequestStatus}) {

    //Log me out
    if(!localStorage.getItem('wonderLustToken')){
        window.location.href = '/'
    }

    useEffect(() => {
        getRequestStatus('get', 'requests/traveller', GET_REQUESTS_STATUS)
    }, [])

    return (
        <Layout>            
            <Navbar 
                names={`${GetUserInfo()?.user?.firstName} ${GetUserInfo()?.user?.lastName}`} 
                role={`${GetUserInfo()?.user?.role === 'traveller' ? '' :
                    GetUserInfo()?.user?.role === 'travelAdmin' ? 'Travel admin' : 'Super admin'
                    }`}
                RequestsMenu={GetUserInfo()?.user?.role === 'traveller' ? RequestsMenu : ''} 
                Profile_Picture={Profile_Picture} 
            />
            
            <Content className="contents">
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item><Link to="/places">Explore</Link></Breadcrumb.Item>
                    <Breadcrumb.Item>Requests</Breadcrumb.Item>
                </Breadcrumb>

                <p className="title_2">{requestStatus.data.length} Trip Requests</p>
                <div className="all_cards">

                {
                    requestStatus.isLoading &&
                        <div style={{
                            width: '100%', 
                            height: '50vh',
                            display: 'flex',
                            alignContent: 'center',
                            alignItems: 'center'
                        }}><Spin style={{margin: 'auto', display: 'block'}} /></div>
                   
                }

                    {
                        requestStatus?.data && requestStatus?.data?.length && requestStatus?.data.map(trip => {
                            return(
                                <SingleCard 
                                    key={trip.id}
                                    status={trip.status} 
                                    name={trip.place.title} 
                                    price={trip.place.price} 
                                    id={trip.place.id} 
                                    picture={trip.place.picture}
                                />
                            )
                        })
                    }
                </div>

            </Content>
        </Layout>
    )
}

const mapState = ({ requestStatus }) => ({requestStatus});
export default connect(mapState, {
    getRequestStatus: apiAction
})(RequestsStatus)
