import React from 'react'
import { Input, Spin } from 'antd/lib';
import { EyeInvisibleOutlined, EyeTwoTone  } from '@ant-design/icons';
import Logo from '../../assets/img/logo.png'
import { Link } from 'react-router-dom'
import './index.css'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { connect } from 'react-redux';
import apiAction from '../../redux/actions/apiAction';
import { USER_LOGIN } from '../../redux/types'

function Login({login, auth}) {

    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: Yup.object({
            email: Yup.string().email().required('Required'),
            password: Yup.string().required('Required')
        }),
        onSubmit: values => {          
            login('post', 'auth/login', USER_LOGIN, JSON.stringify(values) )
        }
    })

    return (
        <div className="login">
            <div className="loginform">

                <div className="login_logo">
                    <img src={Logo} alt="Logo" />
                </div>
                <form onSubmit={formik.handleSubmit}>
                    <Input 
                        {...formik.getFieldProps('email')}
                        placeholder="Email" 
                        className="input" 
                        type="email"
                        name="email"
                    />
                    {formik.touched.email && formik.errors.email ?
                    <span className="error_msg">{formik.errors.email}</span> : null}

                    <Input.Password
                        {...formik.getFieldProps('password')}
                        name="password"
                        className="input"
                        placeholder="Password"
                        iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                    />
                    {formik.touched.password && formik.errors.password ?
                    <span className="error_msg">{formik.errors.password}</span> : null}

                    <button type="submit" className="btn_primary bg_orange">Login</button>
                </form>

                <Link to="/signup">
                    <p className="title_6 text_center text_white_green">Or create new account</p>
                </Link>

                <Link to="/forgotpwd">
                    <p className="title_5 text_center">Forgot password?</p>
                </Link>
                {
                    auth.isLoading && <Spin style={{margin: 'auto', display: 'block'}} />
                }
            </div>
        </div>
    )
}

const mapState = ({ auth }) => ({auth});
export default connect(mapState, {
    login: apiAction,
})(Login)
