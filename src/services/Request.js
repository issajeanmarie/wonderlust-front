import axios from "axios";

// const token = authToken.getToken();

export const instance = axios.create({
  baseURL: "https://wanderlust-ij.herokuapp.com/api/",
  responseType: "json",
  headers: {
    "Authorization": `Bearer ${localStorage.getItem('wonderLustToken')}`,
    "Content-type": "application/json"
  }
});

instance.CancelToken = axios.CancelToken;
instance.isCancel = axios.isCancel;
instance.Cancel = axios.Cancel;

const successResponse = (response) => {
  return response.data;
};

const failResponse = (error) => {
  return Promise.reject(error);
};

const Request = (options) =>
  instance(options).then(successResponse).catch(failResponse);

export default Request;
