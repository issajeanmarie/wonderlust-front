import {
    USER_REGISTER
  } from "../types";
  
  const initialState = {
    isLoading: false,
    data: {}
  };
  
  const registerReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case USER_REGISTER:
        return {
          ...state,
          isLoading: payload?.isLoading ? true : false,
          data: payload
        };
      default:
        return state;
    }
  };

  export default registerReducer
  