import { REQUEST_TRIP } from "../types";

  const initialState = {
    isLoading: false,
    data: {}
  };
  
  const requestTripReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case REQUEST_TRIP:
        return {
          ...state,
          isLoading: payload?.isLoading ? true : false,
          data: payload
        };
      default:
        return state;
    }
  };

  export default requestTripReducer
  