import {
    DELETE_USER
  } from "../types";
  
  const initialState = {
    isLoading: false,
    data: {}
  };
  
  const deleteReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case DELETE_USER:
        return {
          ...state,
          isLoading: payload?.isLoading ? true : false,
          data: payload
        };
      default:
        return state;
    }
  };

  export default deleteReducer
  