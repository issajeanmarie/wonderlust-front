import {
    GET_REQUESTS
  } from "../types";
  
  const initialState = {
    isLoading: false,
    data: {}
  };
  
  const requestsReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case GET_REQUESTS:
        return {
          ...state,
          isLoading: payload?.isLoading ? true : false,
          data: payload
        };
      default:
        return state;
    }
  };
  
  export default requestsReducer