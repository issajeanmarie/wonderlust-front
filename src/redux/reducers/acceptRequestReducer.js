import {
    ACCEPT_REQUEST
  } from "../types";
  
  const initialState = {
    isLoading: false,
    data: {}
  };
  
  const acceptRequestReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case ACCEPT_REQUEST:
        return {
          ...state,
          isLoading: payload?.isLoading ? true : false,
          data: payload
        };
      default:
        return state;
    }
  };

export default acceptRequestReducer
  