import {
    GET_ADMINS
  } from "../types";
  
  const initialState = {
    isLoading: false,
    data: {}
  };
  
  const adminsReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case GET_ADMINS:
        return {
          ...state,
          isLoading: payload?.isLoading ? true : false,
          data: payload
        };
      default:
        return state;
    }
  };

  export default adminsReducer
  