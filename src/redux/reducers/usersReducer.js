import {
    GET_USERS
  } from "../types";
  
  const initialState = {
    isLoading: false,
    data: {}
  };
  
  const usersReducer = (state = initialState, { type, payload }) => {
    switch (type) {
      case GET_USERS:
        return {
          ...state,
          isLoading: payload?.isLoading ? true : false,
          data: payload
        };
      default:
        return state;
    }
  };

  export default usersReducer
  