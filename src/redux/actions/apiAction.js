import { toast } from "react-toastify";
import HttpRequest from "../../services/HttpRequest";
import creator from "./creator";
import jwt_decode from "jwt-decode";

const apiAction = (method, endpoint, actionType, data) => async (dispatch) => {
  try {
    dispatch(creator(actionType, {isLoading: true}));
    const res = await HttpRequest[method](endpoint, data);

    if(res.message === 'user successfully created') {
      window.location.href = '/'
    }

    //Keep to localstorage
    if(res?.data?.accessToken) {
      localStorage.setItem('wonderLustToken', res.data.accessToken) 
      let userData = jwt_decode(res.data.accessToken)
      if (userData?.user?.role === 'traveller'){
          window.location.href = window.location.href+'places'
       }else if(userData?.user?.role === 'travelAdmin'){
          window.location.href = window.location.href+'requests'
       } else{
         window.location.href = window.location.href+'super_admin'
       }
    }

    dispatch(creator(actionType, res.data));
  } catch (e) {
    return toast.error("Something went wrong ");
    // if (e.response && e.response.data) {
    //   dispatch(creator(actionType, e.response.data.error));
    //   return toast.error(e.response.data.error);
    // }
  }
};

export default apiAction