import Login from '../views/views'
import {     
    Register,
    Forgot_Password, 
    Reset_Password,
    Admins,
    Places,
    RequestsStatus,
    Requests
} from '../views/views';
import { BrowserRouter, Switch, Route } from 'react-router-dom'

const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact>
                    <Login />
                </Route>
                <Route path="/signup" exact component={Register}></Route>
                <Route path="/forgotpwd" exact component={Forgot_Password}></Route>
                <Route path="/resetpwd" exact component={Reset_Password}></Route>
                <Route path="/super_admin" component={Admins} />
                <Route path="/places" component={Places} />
                <Route path="/requests_status" component={RequestsStatus} />
                <Route path="/requests" component={Requests} />
            </Switch>
        </BrowserRouter>
    )
}

export default Routes