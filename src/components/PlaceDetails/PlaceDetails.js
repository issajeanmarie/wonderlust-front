import {useEffect} from 'react'
import { Layout, Breadcrumb, Spin } from 'antd/lib';
import './index.css'
import Trip_View from '../TripView'
import { RightOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'
import apiAction from '../../redux/actions/apiAction';
import { connect } from 'react-redux';
import { GET_PLACES, GET_PLACE_DETAILS, REQUEST_TRIP } from '../../redux/types';
import SingleCard from '../../utils/card';
const {  Content } = Layout;

const PlaceDetails = ({places, getPlaces, fetchPlaceDetails, getPlaceDetails, requestForTrip}) => {

    const location = window.location.href.split('/')
    const placeId = location[location.length -1]

    useEffect(() => {
        fetchPlaceDetails('get', `places/${placeId}`, GET_PLACE_DETAILS)
        getPlaces('get', 'places', GET_PLACES)
    },[])

    const result = !getPlaceDetails?.data?.isLoading && getPlaceDetails?.data
    const placesResult = !places.isLoading && places.data.length >= 1 ? places.data : []

    const requestAtrip = e => {
        requestForTrip('post', `requests/${placeId}`, REQUEST_TRIP)
    }

    const button = <button 
                        onClick={() => requestAtrip(result.id)} 
                        className="request_button"
                    >
                        Request trip 
                    <RightOutlined />
                    </button>

    return (
        <>
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>
                <Link to="/places">Explore</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Requests</Breadcrumb.Item>
            </Breadcrumb>
            {getPlaceDetails.data.isLoading && <Spin />}
            <Content>            

                <Trip_View 
                    img={result?.picture} 
                    title={result?.title}
                    price={result?.price} 
                    desc={result?.description}
                    footer={button }
                />

                <br />
                <p className="title_2">More places</p>
                <br />

                <div className="all_cards">
                    {
                        placesResult.map(place => {
                            return(
                                <SingleCard
                                    key={place.id} 
                                    status="" 
                                    name={place.title}  
                                    price={place.price}
                                    id={place.id}
                                    picture={place.picture}
                                />
                            )
                        })
                    }
                </div>

            </Content>
        </>
    )
}

const mapState = ({ getPlaceDetails, places }) => ({getPlaceDetails, places});
export default connect(mapState, {
    fetchPlaceDetails: apiAction,
    requestForTrip: apiAction,
    getPlaces: apiAction
})(PlaceDetails)