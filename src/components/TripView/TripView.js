import './index.css'

const TripView = ({img, title, price, desc, footer}) => {
    return(
        <div className="trip_view">
            <div className="trip_view_cover">
                <img src={img} alt="profile"/>
            </div>
            <div className="trip_view_desc">
                <p style={{fontSize: '20px'}}>{title}</p>
                <p className="title_2" style={{marginTop: '-17px'}}>{price?.toLocaleString()} Rwf</p>
                <br />

                <p className="title_5">Description</p>
                <p>{desc}</p>
                <br />

                {footer}
            </div>
        </div>
    )
}

export default TripView