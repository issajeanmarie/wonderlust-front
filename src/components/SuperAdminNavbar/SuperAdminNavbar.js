import React from 'react'
import './index.css'
import { Link } from 'react-router-dom'

export function SuperAdminNavbar() {

    const url = window.location.href.split('/')
    const to = url[url.length - 1]

    return (
        <>
            <div className="super_admin_nav">
                <div className="menu">
                    <Link to="/super_admin" >
                        <span className={to !== 'users' ? 'active' : null}>trip admins</span>
                    </Link>
                    <Link to="/super_admin/users" >
                        <span className={to === 'users' ? 'active' : null}>users</span>
                    </Link>
                </div>
            </div>
        </>
    )
}
