import React from 'react'
import { ClockCircleOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'
import './index.css'

export const RequestsMenu = () => {

    const url = window.location.href.split('/')
    const to = url[url.length - 1]

    return(
        <Link to="/requests_status">
            <span 
                className="bold request" style={{color: to === 'requests_status' ? `#F15A24` : '#333'}}
            >
                <ClockCircleOutlined className="request_icon" /> <span className="request_text">Requests</span>
            </span>
        </Link>
    )
}