import { Spin } from 'antd/lib';
import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import apiAction from '../../redux/actions/apiAction';
import { GET_PLACES } from '../../redux/types';
import SingleCard from '../../utils/card'

function ShowPlaces({places, getPlaces}) {

    useEffect(() => {
        getPlaces('get', 'places', GET_PLACES)
    }, [])

    return (
        <>
            <br />
            <p className="title_2">Explore {places.data.length} places</p>
            <br />

            <div className="all_cards">

                {places.data.isLoading && <Spin style={{display: 'block', margin: '10% auto'}} />}

                {
                    places?.data && places?.data?.length && places?.data.map(place => {
                        return(
                            <SingleCard
                                key={place.id} 
                                status="" 
                                name={place.title}  
                                price={place.price}
                                id={place.id}
                                picture={place.picture}
                            />
                        )
                    })
                }
            </div>

        </>
    )
}

const mapState = ({ places }) => ({places});
export default connect(mapState, {
    getPlaces: apiAction
})(ShowPlaces)
