import React from 'react'
import './index.css'
import { StarFilled } from '@ant-design/icons'
import { connect } from 'react-redux';
import apiAction from '../../redux/actions/apiAction';
import changeRole from '../../utils/ChangeRole';

function Users({allUsers, changeUserRole}) {
    return (
        <>
            <div className="trip_admins">
                {
                    allUsers.map(user => {
                        return(
                            <div className="single_admin" key={user.id}>
                                <div className="trip_admin_profile"></div>
                                <div className="title_4 small">{user.firstName} {user.lastName}</div>
                                <div className="text_label bold small">{user.email}</div>
                                <div className="role">
                                    Role: 
                                    <span 
                                        className={user.role === 'traveller' ? 'bold' : 'bold isAdmin'} 
                                        style={{textTransform: 'uppercase'}}
                                    >
                                        {user.role !== 'traveller' ? 'Trip Admin' : 'Regular user'}
                                    </span>
                                </div>
                                <div className="request_options">
                                    <button 
                                        className={user.role === 'traveller' ? 'star_button' : 'star_button checked'}
                                        onClick={() => changeRole(user.email, user.role, changeUserRole)}
                                    >
                                        {<StarFilled />}
                                    </button>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
            
        </>
    )
}

const mapState = ({ users, role }) => ({users, role});
export default connect(mapState, {
    getUsers: apiAction
})(Users)
