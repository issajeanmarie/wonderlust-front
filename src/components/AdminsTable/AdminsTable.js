import React from 'react'
import './index.css'
import { DeleteFilled } from '@ant-design/icons/lib/icons'
import { deleteAdmin } from '../../utils/deleteUser'

function AdminsTable({admins, deletePerson}) {

    return (
        <>
            <div className="trip_admins">
                {
                    admins.map(admin => {
                        return(
                            <div className="single_admin" key={admin.id}>
                                <div className="trip_admin_profile"></div>
                                <div className="title_4 small admin_name">{admin.firstName} {admin.lastName}</div>
                                <div className="text_label bold small">{admin.email}</div>
                                <div className="request_options">
                                    <button onClick={() => deleteAdmin(admin.id, deletePerson)} className="delete_button"><DeleteFilled /></button>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
            
        </>
    )
}

export default AdminsTable