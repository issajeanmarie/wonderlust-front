import React, { useState } from 'react'     
import './index.css'
import { CheckOutlined } from '@ant-design/icons'
import { ACCEPT_REQUEST, DELETE_REQUEST } from '../../redux/types'

export function SingleRequest({
    name, 
    picture, 
    price, 
    user_email, 
    user_name, 
    id, 
    approveRequest,
    deleteRequest,
    status
}) {
    const [isChecked, setIsChecked] = useState(false)

    const addRequest = (id, approveRequest) => {
        approveRequest('patch', `requests/approve/${id}`, ACCEPT_REQUEST)
        setIsChecked(!isChecked)
    }

    const rejectRequest = (id, deleteRequest) => {
        approveRequest('patch', `requests/reject/${id}`, DELETE_REQUEST)
    }

    return (
        <>
            <div className={isChecked ? 'single_trip_request checked' : 
                            'single_trip_request' && status === 'APPROVED' ? 
                            'single_trip_request checked' : 'single_trip_request'
                            }>
                <div className="request_img">
                    <img src={picture} alt="request profile"/>
                </div>
                <div className="trip_names">
                    <p className="text_label bold">{name}</p>
                    <p className="title_3 small">{price?.toLocaleString()} Rwf</p>
                </div>
                <div className="trip_names">
                    <p className="title_3 small">{user_name}</p>
                    <p className="text_label bold">{user_email}</p>
                </div>
                <div className="request_options">
                    <button className="delete_button" onClick={() => rejectRequest(id, deleteRequest)}>X</button>
                    <button className="tick_button" onClick={() => addRequest(id, approveRequest)}><CheckOutlined /></button>
                </div>
            </div>
        </>
    )
}
