import React, {useState} from 'react'
import './index.css'
import { CaretDownOutlined } from '@ant-design/icons'
import { Popover } from 'antd/lib';
import Logout from '../../utils/Logout';

export const Profile = ({names, role, RequestsMenu, Profile_Picture}) => {

    const [isVisible, setIsVisible] = useState(false)

    const content = (
        <div>
          <p 
            onClick={() => Logout()}
            className="logout_button" 
           >
                Logout
            </p>
        </div>
      );

    return(
        <div className="profile_info">
            <div className="profile_cont">
                
                {RequestsMenu ? <RequestsMenu /> : null}

                <div className="profile_img">
                    <img src={Profile_Picture} alt="profile"/>
                </div>
                <Popover 
                    content={content} 
                    visible={isVisible} 
                    trigger="click"
                    onVisibleChange={() => setIsVisible(!isVisible)}
                    
                >
                    <div style={{cursor: 'pointer'}}>
                        <span className={"title_4 names "+role}>{names} 
                            <CaretDownOutlined />
                        </span>
                        {role ? <span className="role_text">{role}</span> : null}
                    </div>
                </Popover>
            </div>
        </div>
    )
}