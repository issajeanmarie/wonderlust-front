import React from 'react'
import './index.css'
import Logo from '../../assets/img/Group 5.png';
import { Profile } from '../profile'
import { Layout } from 'antd/lib';
import { GetUserInfo } from '../../utils/GetUserInfo';
import Profile_Picture from '../../assets/img/gollira.jpg'
import { RequestsMenu } from '../../components'

const { Header } = Layout;
export const Navbar  = () => {

    return(
        <Header className="nav_header">
        <div className="navbar">
            <div className="brand_logo">
                <img src={Logo} alt="logo" />
                <span className="text_label title_5">WanderLust</span>
            </div>            
            <Profile 
                names={`${GetUserInfo()?.firstName} ${GetUserInfo()?.lastName}`} 
                role={`${GetUserInfo()?.role === 'traveller' ? '' :
                GetUserInfo()?.role === 'travelAdmin' ? 'Travel admin' : 'Super admin'
                }`} 
                RequestsMenu={GetUserInfo()?.role === 'traveller' ? RequestsMenu : ''} 
                Profile_Picture={Profile_Picture}
            >
            </Profile>
        </div>
        </Header>
    )
}